package ru.appliedtech.timetracer.processor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.util.ReflectionUtils;
import ru.appliedtech.timetracer.annotation.impl.TimeTrace;
import ru.appliedtech.timetracer.config.TimeTracerConfig;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class TimeTracerPostProcessor implements BeanPostProcessor {

    Map<String,Class<?>> classWithTimeTrace = new HashMap<>();

    @Autowired
    TimeTracerConfig config;

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName){
        //log.debug(" ------ Time trace initialization ----- count classes {}", classWithTimeTrace.size());
        for (Method m : ReflectionUtils.getAllDeclaredMethods(bean.getClass())){
            if (m.isAnnotationPresent(TimeTrace.class)){
                classWithTimeTrace.put(beanName, bean.getClass());
            }
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName){
        final Class<?> clazz = classWithTimeTrace.get(beanName);
        if (clazz != null){
            return Proxy.newProxyInstance(
                    clazz.getClassLoader(),
                    clazz.getInterfaces(),
                    new InvocationHandler() {
                        @Override
                        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                            if (config.isEnabled()&&clazz.getDeclaredMethod(method.getName(), method.getParameterTypes()).isAnnotationPresent(TimeTrace.class)){
                                Long timestampBefore = System.nanoTime();
                                Object res = method.invoke(bean,args);
                                Long timestampAfter = System.nanoTime();
                                log.debug("Time for method {} is {}",method.getName(),timestampAfter - timestampBefore);
                                return res;
                            }
                            return method.invoke(bean, args);
                        }
                    }
            );
        }
        return bean;
    };
}

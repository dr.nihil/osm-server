package ru.appliedtech.timetracer.config.impl;

import ru.appliedtech.timetracer.config.TimeTracerConfig;
import ru.appliedtech.timetracer.config.properties.TimeTraceConfigProperties;

import java.util.concurrent.atomic.AtomicBoolean;

public class TimeTracerConfigImpl implements TimeTracerConfig {
    private AtomicBoolean enabled;

    public TimeTracerConfigImpl(TimeTraceConfigProperties props){
        enabled = new AtomicBoolean(props.isEnabled());
    }

    @Override
    public boolean isEnabled() {
        return enabled.get();
    }

    @Override
    public void setEnabled(boolean b) {
        this.enabled.set(b);
    }
}

package ru.appliedtech.timetracer.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "config.timetracer")
@Data
public class TimeTraceConfigProperties {
    private boolean enabled;
}

package ru.appliedtech.timetracer.config;

public interface TimeTracerConfig {
    public boolean isEnabled();
    public void setEnabled(boolean b);
}

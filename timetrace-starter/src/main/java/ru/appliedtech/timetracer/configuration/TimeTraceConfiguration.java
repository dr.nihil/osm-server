package ru.appliedtech.timetracer.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.appliedtech.timetracer.config.TimeTracerConfig;
import ru.appliedtech.timetracer.config.impl.TimeTracerConfigImpl;
import ru.appliedtech.timetracer.config.properties.TimeTraceConfigProperties;
import ru.appliedtech.timetracer.processor.TimeTracerPostProcessor;

@Configuration
@EnableConfigurationProperties(TimeTraceConfigProperties.class)
public class TimeTraceConfiguration {
    @Autowired
    TimeTraceConfigProperties props;

    @Bean
    TimeTracerConfig timeTracerConfig(){
        return new TimeTracerConfigImpl(props);
    }

    @Bean
    TimeTracerPostProcessor timeTracerPostProcessor(){
        return new TimeTracerPostProcessor();
    }
}

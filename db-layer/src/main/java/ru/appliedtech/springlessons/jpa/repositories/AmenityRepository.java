package ru.appliedtech.springlessons.jpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.appliedtech.springlessons.buisnesobjects.NodeAmenity;

import java.util.Set;

public interface AmenityRepository extends JpaRepository<NodeAmenity, Long> {
    @Query("select id from NodeAmenity where name = :name")
    public Long getIdByName(String name);

    @Query("select a from NodeAmenity a where a.name in :names")
    public Set<NodeAmenity> findAllByNames(Set<String> names);

    public NodeAmenity findByName(String name);
}

package ru.appliedtech.springlessons.jpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import ru.appliedtech.springlessons.buisnesobjects.WayObject;

@Transactional
public interface WayRepository extends JpaRepository<WayObject, Long> {
    @Query("select id from WayObject where native_id = :nativeId")
    public Long getIdByNativeId(Long nativeId);
}

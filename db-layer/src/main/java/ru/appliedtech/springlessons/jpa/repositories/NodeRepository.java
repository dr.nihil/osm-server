package ru.appliedtech.springlessons.jpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import ru.appliedtech.springlessons.buisnesobjects.NodeObject;

@Transactional
public interface NodeRepository extends JpaRepository<NodeObject, Long> {
    @Query("select id from NodeObject where native_id = :nativeId")
    public Long getIdByNativeId(Long nativeId);
}

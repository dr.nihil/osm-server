package ru.appliedtech.springlessons.jpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import ru.appliedtech.springlessons.buisnesobjects.RelationObject;

@Transactional
public interface RelationRepository extends JpaRepository<RelationObject, Long> {
    @Query("select id from RelationObject where native_id = :nativeId")
    public Long getIdByNativeId(Long nativeId);
}

package ru.appliedtech.springlessons.dao.dataload;

public interface DataloadDAO<T> {
    public String getType();

    public void saveInBatch(T object);

    public void flushBatch();
}

package ru.appliedtech.springlessons.dao.impl.jpa;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.appliedtech.springlessons.buisnesobjects.NodeAmenity;
import ru.appliedtech.springlessons.buisnesobjects.NodeObject;
import ru.appliedtech.springlessons.dao.DAOService;
import ru.appliedtech.springlessons.dao.dataload.DataloadDAO;
import ru.appliedtech.springlessons.jpa.repositories.AmenityRepository;
import ru.appliedtech.springlessons.jpa.repositories.NodeRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service("JpaNode")
@Qualifier("jpa")
@Slf4j
@Transactional
public class JpaNodeService extends AbstractDAOJpa implements DAOService<NodeObject>, DataloadDAO<NodeObject> {

    private final NodeRepository nodeRepository;
    private final AmenityRepository amenityRepository;

    private final String type = "node";
    private List<NodeObject> nodeBatch = new ArrayList<>();

    public JpaNodeService(NodeRepository nodeRepository, AmenityRepository amenityRepository) {
        this.nodeRepository = nodeRepository;
        this.amenityRepository = amenityRepository;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public NodeObject save(NodeObject node) {
        node.setId(nodeRepository.getIdByNativeId(node.getNativeId()));
        NodeAmenity amenity = node.getAmenity();
        if (amenity != null) {
            NodeAmenity dbAmenity = amenityRepository.findByName(amenity.getName());
            if (dbAmenity != null) {
                node.setAmenity(dbAmenity);
            }
        }
        return nodeRepository.save(node);
    }

    @Override
    public void saveInBatch(NodeObject object) {
        nodeBatch.add(object);
        if (nodeBatch.size() >= BATCH_SIZE) {
            flushBatch();
        }
    }

    @Override
    public void flushBatch() {
        saveAll(nodeBatch);
        nodeBatch.clear();
    }

    @Override
    public List<NodeObject> saveAll(List<NodeObject> nodes) {
        Set<NodeAmenity> amenities = nodes.stream()
                .map(NodeObject::getAmenity)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        if (!amenities.isEmpty()) {
            Set<NodeAmenity> existedAmenities = amenityRepository.findAllByNames(amenities.stream()
                    .map(NodeAmenity::getName)
                    .collect(Collectors.toSet()));
            amenities.removeAll(existedAmenities);
            if (!amenities.isEmpty()) {
                existedAmenities.addAll(amenityRepository.saveAll(amenities));
            }

            nodes.forEach(node -> {
                NodeAmenity amenity = node.getAmenity();
                if (amenity != null) {
                    existedAmenities.stream()
                            .filter(a -> a.getName().equals(amenity.getName()))
                            .findFirst()
                            .ifPresent(node::setAmenity);
                }
            });
        }
        return nodeRepository.saveAll(nodes);
    }

    @Override
    public Optional<NodeObject> getById(Long id) {
        Optional<NodeObject> node = nodeRepository.findById(id);
        log.debug("Got node: {} ", node.orElse(null));
        return node;
    }

    @Override
    public void remove(Long id) {
        nodeRepository.deleteById(id);
        log.debug("Removed node with id: {}", id);
    }

    @Override
    public NodeObject update(NodeObject node) {
        NodeObject updatedNode = nodeRepository.save(node);
        log.debug("Node is updated: {}", node);
        return updatedNode;
    }
}

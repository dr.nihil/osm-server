package ru.appliedtech.springlessons.dao.impl.jpa;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.appliedtech.springlessons.buisnesobjects.WayObject;
import ru.appliedtech.springlessons.dao.DAOService;
import ru.appliedtech.springlessons.dao.dataload.DataloadDAO;
import ru.appliedtech.springlessons.jpa.repositories.WayRepository;

import java.util.ArrayList;
import java.util.List;

@Service("JpaWay")
@Qualifier("jpa")
public class JpaWayService extends AbstractDAOJpa implements DAOService<WayObject>, DataloadDAO<WayObject> {
    private final String type = "way";
    private List<WayObject> wayBatch = new ArrayList<>();
    private final WayRepository wayRepository;

    public JpaWayService(WayRepository wayRepository) {
        this.wayRepository = wayRepository;
    }

    @Override
    public String getType(){
        return type;
    }

    @Override
    public WayObject save(WayObject way) {
        way.setId(wayRepository.getIdByNativeId(way.getNativeId()));
        return wayRepository.save(way);
    }

    @Override
    public List<WayObject> saveAll(List<WayObject> ways) {
        return wayRepository.saveAll(ways);
    }

    @Override
    public void saveInBatch(WayObject object) {
        wayBatch.add(object);
        if (wayBatch.size() >= BATCH_SIZE){
            flushBatch();
        }
    }

    @Override
    public void flushBatch() {
        saveAll(wayBatch);
        wayBatch.clear();
    }
}

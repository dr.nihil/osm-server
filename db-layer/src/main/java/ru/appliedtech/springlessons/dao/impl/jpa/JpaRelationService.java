package ru.appliedtech.springlessons.dao.impl.jpa;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.appliedtech.springlessons.buisnesobjects.RelationObject;
import ru.appliedtech.springlessons.dao.DAOService;
import ru.appliedtech.springlessons.dao.dataload.DataloadDAO;
import ru.appliedtech.springlessons.jpa.repositories.RelationRepository;

import java.util.ArrayList;
import java.util.List;

@Service("JpaRelation")
@Qualifier("jpa")
public class JpaRelationService extends AbstractDAOJpa implements DAOService<RelationObject>, DataloadDAO<RelationObject> {

    private final String type = "relation";
    private List<RelationObject> relationBatch = new ArrayList<>();
    private final RelationRepository relationRepository;

    public JpaRelationService(RelationRepository relationRepository) {
        this.relationRepository = relationRepository;
    }

    @Override
    public String getType(){
        return type;
    }

    @Override
    public RelationObject save(RelationObject relation) {
        relation.setId(relationRepository.getIdByNativeId(relation.getNativeId()));
        return relationRepository.save(relation);
    }

    @Override
    public void saveInBatch(RelationObject object) {
        relationBatch.add(object);
        if (relationBatch.size() >= BATCH_SIZE){
            flushBatch();
        }
    }

    @Override
    public void flushBatch() {
        saveAll(relationBatch);
        relationBatch.clear();
    }

    @Override
    public List<RelationObject> saveAll(List<RelationObject> relations) {
        return relationRepository.saveAll(relations);
    }
}

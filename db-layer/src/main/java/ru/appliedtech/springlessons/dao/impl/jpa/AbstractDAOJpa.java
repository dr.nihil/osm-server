package ru.appliedtech.springlessons.dao.impl.jpa;

import org.springframework.beans.factory.annotation.Value;

public abstract class AbstractDAOJpa {
    @Value("${spring.jpa.properties.hibernate.jdbc.batch_size}")
    Integer BATCH_SIZE;
    private final String type = "abstract";

    public String getType() {
        return type;
    }

    public Integer getBatchSize(){
        return BATCH_SIZE;
    }
}

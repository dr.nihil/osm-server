package ru.appliedtech.springlessons.dao;

import java.util.List;
import java.util.Optional;

public interface DAOService<T> {
    public T save(T object);

    default public void get() {
        throw new RuntimeException("Method is unsupported");
    }

    default public List<T> saveAll(List<T> objects) {
        throw new RuntimeException("Method is unsupported");
    }

    default public Optional<T> getById(Long id) {
        throw new RuntimeException("Method is unsupported");
    }

    default public void remove(Long id) {
        throw new RuntimeException("Method is unsupported");
    }

    default public T update(T object) {
        throw new RuntimeException("Method is unsupported");
    }

    default Long getIdByName(String name) {
        throw new RuntimeException("Method is unsupported");
    }

    public Integer getBatchSize();
}

package ru.appliedtech.springlessons.dao.impl.jpa;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.appliedtech.springlessons.buisnesobjects.NodeAmenity;
import ru.appliedtech.springlessons.dao.DAOService;
import ru.appliedtech.springlessons.jpa.repositories.AmenityRepository;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service("AmenityService")
@Qualifier("jpa")
public class AmenityService extends AbstractDAOJpa implements DAOService<NodeAmenity> {

    AmenityRepository amenityRepository;

    public AmenityService(AmenityRepository amenityRepository){
        this.amenityRepository = amenityRepository;
    }

    @Override
    public NodeAmenity save(NodeAmenity object) {
        return amenityRepository.save(object);
    }

    @Override
    public List<NodeAmenity> saveAll(List<NodeAmenity> objects) {
        return DAOService.super.saveAll(objects);
    }

    @Override
    public Optional<NodeAmenity> getById(Long id) {
        return amenityRepository.findById(id);
    }

    @Override
    public void remove(Long id) {
       amenityRepository.deleteById(id);
    }

    @Override
    public NodeAmenity update(NodeAmenity object) {
        return amenityRepository.save(object);
    }

    public Long getIdByName(String name){
        return amenityRepository.getIdByName(name);
    }
}

package ru.appliedtech.springlessons.buisnesobjects;

import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="way")
@Data
public class WayObject extends BaseObject{
    @Id
    @Column(name="id")
    @SequenceGenerator(name = "way_seq", sequenceName = "way_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "way_seq")
    private Long id;

    @Type(type="jsonb")
    @Column(name="nd_ref", columnDefinition = "jsonb")
    List<Long> ndRef;

    @Type(type="jsonb")
    @Column(name="tags", columnDefinition = "jsonb")
    List<TagObject> tags;
}

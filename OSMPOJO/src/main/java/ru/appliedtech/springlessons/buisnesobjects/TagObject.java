package ru.appliedtech.springlessons.buisnesobjects;

import lombok.Data;
import java.io.Serializable;

@Data
public class TagObject implements Serializable {

    private static final long serialVersionUID = 1L;

    private String k;
    private String v;
}

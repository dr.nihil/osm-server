package ru.appliedtech.springlessons.buisnesobjects;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "amenity")
public class NodeAmenity implements Serializable {

    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "amenity_seq", sequenceName = "amenity_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "amenity_seq")
    private Long id;

    @Column(name = "name")
    private String name;

    public NodeAmenity(){
    }

    public NodeAmenity(String name){
        this.name = name;
    }

    @Override
    public boolean equals(Object object){
        if (object instanceof NodeAmenity) {
            return this.getName().equals(((NodeAmenity) object).getName());
        }
        return false;
    }

    @Override
    public int hashCode(){
        return this.getName().hashCode();
    }
}

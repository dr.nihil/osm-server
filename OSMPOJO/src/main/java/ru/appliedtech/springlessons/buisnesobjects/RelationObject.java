package ru.appliedtech.springlessons.buisnesobjects;

import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name= "relation")
@Data
public class RelationObject extends BaseObject{
    @Id
    @Column(name="id")
    @SequenceGenerator(name = "rel_seq", sequenceName = "rel_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rel_seq")
    private Long id;

    @Type(type="jsonb")
    @Column(name="tags", columnDefinition = "jsonb")
    List<TagObject> tags;
    @Type(type="jsonb")
    @Column(name="members", columnDefinition = "jsonb")
    List<MemberObject> members;
}

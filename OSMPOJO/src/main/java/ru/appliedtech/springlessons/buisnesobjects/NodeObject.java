package ru.appliedtech.springlessons.buisnesobjects;

import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "node")
@Data
public class NodeObject extends BaseObject {
    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "node_seq", sequenceName = "node_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "node_seq")
    private Long id;

    @Column(name = "lat")
    private Float lat;

    @Column(name = "lon")
    private Float lon;

    @Type(type = "jsonb")
    @Column(name = "tags", columnDefinition = "jsonb")
    private List<TagObject> tags;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "amenity_id")
    private NodeAmenity amenity;

    @Column(name = "d1", insertable = false, updatable = false)
    private Double d1;

    @Column(name = "d2", insertable = false, updatable = false)
    private Double d2;
}

package ru.appliedtech.springlessons.buisnesobjects;

import lombok.Data;

import java.io.Serializable;

@Data
public class MemberObject implements Serializable {

    private static final long serialVersionUID = 1L;

    String type;
    Long ref;
    String role;
}

package ru.appliedtech.springlessons.buisnesobjects;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.Data;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@MappedSuperclass
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class BaseObject implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name="nativeId")
    private Long nativeId;
    @Column(name="version")
    private Integer version;
    @Column(name="timestamp")
    private Date timestamp;
}

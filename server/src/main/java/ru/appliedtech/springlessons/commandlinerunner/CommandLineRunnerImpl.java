package ru.appliedtech.springlessons.commandlinerunner;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import ru.appliedtech.springlessons.processor.impl.IProcessor;

@Slf4j
@Service
public class CommandLineRunnerImpl implements CommandLineRunner {

    private IProcessor processor;

    public CommandLineRunnerImpl (@Qualifier("XmlProcessor") IProcessor processor){
        this.processor = processor;
    }

    @Override
    public void run(String... args) throws Exception {
        Options options = new Options();
        options.addOption("file",true, "Path to file");
        options.addOption("clean",false, "Define for clean tables");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            log.error("Error during parsing options", e);
            throw e;
        }
        processor.process(cmd.getOptionValue("file"));
    }
}

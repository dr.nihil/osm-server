package ru.appliedtech.springlessons;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import ru.appliedtech.springlessons.commandlinerunner.CommandLineRunnerImpl;

@SpringBootApplication
@ComponentScan(basePackages = {"ru.appliedtech.springlessons"},excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = CommandLineRunnerImpl.class))
public class SpringlessonsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringlessonsApplication.class, args);
	}

}

package ru.appliedtech.springlessons.rest.controllers;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.appliedtech.springlessons.buisnesobjects.NodeAmenity;
import ru.appliedtech.springlessons.buisnesobjects.NodeObject;
import ru.appliedtech.springlessons.dao.DAOService;

import java.util.NoSuchElementException;

@RestController
@RequestMapping("/nodes")
public class NodeController {

    private DAOService<NodeObject> nodeService;

    public NodeController(@Qualifier("JpaNode") DAOService<NodeObject> nodeService) {
        this.nodeService = nodeService;
    }


    @PostMapping
    ResponseEntity<NodeObject> createNode(@RequestBody NodeObject node){
        return new ResponseEntity<>(nodeService.save(node), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    ResponseEntity<NodeObject> getNode(@PathVariable Long id) {
        return new ResponseEntity <>(nodeService.getById(id)
                .orElseThrow(() -> new NoSuchElementException("Node with id=" + id + " not found")),
                HttpStatus.OK);
    }

    @PutMapping("/{id}")
    ResponseEntity<NodeObject> updateNode(@RequestBody NodeObject node,@PathVariable Long id) {
        return new ResponseEntity <>(nodeService.save(node),
                HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    void deleteNode(@PathVariable Long id){
        nodeService.remove(id);
    }

    @GetMapping("/{id}/amenity")
    ResponseEntity<NodeAmenity> getAmenity(@PathVariable Long id) {
        return new ResponseEntity <>(nodeService.getById(id)
                .orElseThrow(() -> new NoSuchElementException("Node with id=" + id + " not found")).getAmenity(),
                HttpStatus.OK);
    }
}

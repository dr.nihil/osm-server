package ru.appliedtech.springlessons.rest.controllers;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.appliedtech.springlessons.buisnesobjects.WayObject;
import ru.appliedtech.springlessons.dao.DAOService;

import java.util.NoSuchElementException;

@RestController
@RequestMapping("/ways")
public class WayController {

    private DAOService<WayObject> wayService;

    public WayController(@Qualifier("JpaWay") DAOService<WayObject> wayService) {
        this.wayService = wayService;
    }


    @PostMapping
    ResponseEntity<WayObject> createWay(@RequestBody WayObject way){
        return new ResponseEntity<>(wayService.save(way), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    ResponseEntity<WayObject> getWay(@PathVariable Long id) {
        return new ResponseEntity <>(wayService.getById(id)
                .orElseThrow(() -> new NoSuchElementException("Way with id=" + id + " not found")),
                HttpStatus.OK);
    }

    @PutMapping("/{id}")
    ResponseEntity<WayObject> updateWay(@RequestBody WayObject way,@PathVariable Long id) {
        return new ResponseEntity <>(wayService.save(way),
                HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    void deleteWay(@PathVariable Long id){
        wayService.remove(id);
    }
}

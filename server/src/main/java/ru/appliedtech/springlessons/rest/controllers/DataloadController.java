package ru.appliedtech.springlessons.rest.controllers;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.appliedtech.springlessons.processor.impl.IProcessor;

@RestController
@RequestMapping("/dataload")
public class DataloadController {
    IProcessor xmlProcessor;
    Thread loadThread;

    public DataloadController(@Qualifier("XmlProcessor") IProcessor xmlProcessor){
        this.xmlProcessor = xmlProcessor;
    }

    @RequestMapping(value = "/load", method = RequestMethod.POST)
    ResponseEntity<String> loadFromXmlFile(@RequestParam(value="path") String path){
        loadThread = new Thread(() -> xmlProcessor.process(path));
        loadThread.start();
        return new ResponseEntity<>("Dataload is started from file " + path, HttpStatus.OK);
    }
}

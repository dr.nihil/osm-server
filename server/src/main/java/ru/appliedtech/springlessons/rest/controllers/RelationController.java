package ru.appliedtech.springlessons.rest.controllers;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.appliedtech.springlessons.buisnesobjects.RelationObject;
import ru.appliedtech.springlessons.dao.DAOService;

import java.util.NoSuchElementException;

@RestController
@RequestMapping("/relations")
public class RelationController {

    private DAOService<RelationObject> relationService;

    public RelationController(@Qualifier("JpaRelation") DAOService<RelationObject> relationService) {
        this.relationService = relationService;
    }


    @PostMapping
    ResponseEntity<RelationObject> createRelation(@RequestBody RelationObject relation){
        return new ResponseEntity<>(relationService.save(relation), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    ResponseEntity<RelationObject> getRelation(@PathVariable Long id) {
        return new ResponseEntity <>(relationService.getById(id)
                .orElseThrow(() -> new NoSuchElementException("Relation with id=" + id + " not found")),
                HttpStatus.OK);
    }

    @PutMapping("/{id}")
    ResponseEntity<RelationObject> updateRelation(@RequestBody RelationObject relation,@PathVariable Long id) {
        return new ResponseEntity <>(relationService.save(relation),
                HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    void deleteRelation(@PathVariable Long id){
        relationService.remove(id);
    }
}

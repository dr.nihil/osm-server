package ru.appliedtech.springlessons.rest.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.appliedtech.springlessons.monitoring.Monitor;
import ru.appliedtech.springlessons.monitoring.statuses.ILoadStatus;
import ru.appliedtech.springlessons.monitoring.statuses.ServerStatus;

@RestController
@RequestMapping("/status")
public class StatusController {

    private Monitor monitoring;

    public StatusController(Monitor monitoring) {
        this.monitoring = monitoring;
    }

    @GetMapping("/load")
    public ResponseEntity<ILoadStatus> getLoadStatus() {
        return new ResponseEntity<>(monitoring.getLoadStatus(), HttpStatus.OK);
    }

    @GetMapping("/server")
    public ResponseEntity<ServerStatus> getServerStatus() {
        return new ResponseEntity<>(monitoring.getServerStatus(), HttpStatus.OK);
    }
}

package ru.appliedtech.springlessons.processor.impl;

public interface IProcessor {
    public void process(String pathFile);
}

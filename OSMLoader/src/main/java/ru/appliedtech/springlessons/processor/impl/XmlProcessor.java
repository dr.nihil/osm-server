package ru.appliedtech.springlessons.processor.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.appliedtech.springlessons.converter.IConverter;
import ru.appliedtech.springlessons.crutch.xmlreader.NamespaceAddingReader;
import ru.appliedtech.springlessons.dao.dataload.DataloadDAO;
import ru.appliedtech.springlessons.decompressor.impl.IDecompressor;
import ru.appliedtech.springlessons.monitoring.Monitor;

import javax.xml.bind.*;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

@Slf4j
@Service("XmlProcessor")
public class XmlProcessor implements IProcessor {

    private final String jaxbContextPath = "ru.appliedtech.osm.objects";

    IDecompressor decompressor;
    List<DataloadDAO> daoServices;
    IConverter converter;
    Monitor monitor;

    @Autowired
    public XmlProcessor(IDecompressor decompressor,
                        IConverter converter,
                        Monitor monitor,
                        @Qualifier("jpa")
                                List<DataloadDAO> daoServices) {
        this.decompressor = decompressor;
        this.converter = converter;
        this.daoServices = daoServices;
        this.monitor = monitor;
    }

    @Override
    public void process(String filePath) {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
        //try (InputStream in = decompressor.decompressFile(filePath)){
        try (FileInputStream inputStream = new FileInputStream(filePath); BZip2CompressorInputStream xmlFile = new BZip2CompressorInputStream(inputStream)) {
//            if (xmlFile == null) {
//                throw new RuntimeException("InputStream is null");
//            }
            monitor.getLoadStatus().start();
            JAXBContext jc = JAXBContext.newInstance(jaxbContextPath);
            Unmarshaller u = jc.createUnmarshaller();

            NamespaceAddingReader reader = new NamespaceAddingReader(xmlInputFactory.createXMLStreamReader(xmlFile));
            while (reader.hasNext()) {
                if (reader.isStartElement()) {
                    String elementName = reader.getLocalName();
                    try {
                        DataloadDAO daoService = daoServices.stream()
                                .filter(dao -> dao.getType().equals(elementName))
                                .findFirst()
                                .orElse(null);
                        if (daoService != null) {
                            Class<?> clazz = Class.forName(jaxbContextPath + "." + elementName.substring(0, 1).toUpperCase() + elementName.substring(1));
                            daoService.saveInBatch(converter.convert(u.unmarshal(reader, clazz).getValue()));
                            monitor.getLoadStatus().increaseCounter(elementName);
                        } else {
                            log.debug("Can't find DAO for element {}", elementName);
                            reader.next();
                        }
                    } catch (ClassNotFoundException e) {
                        log.debug("Class not found for element {}", elementName);
                        reader.next();
                    }
                } else {
                    reader.next();
                }
            }
            daoServices.forEach(DataloadDAO::flushBatch);
        } catch (XMLStreamException | IOException e) {
            log.error("Can't read input stream", e);
        } catch (JAXBException e) {
            log.error("Error during creating JAXB context", e);
        } finally {
            monitor.getLoadStatus().stop();
        }
    }
}

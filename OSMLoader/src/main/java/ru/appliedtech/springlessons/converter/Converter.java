package ru.appliedtech.springlessons.converter;

import org.springframework.stereotype.Service;
import ru.appliedtech.osm.objects.*;
import ru.appliedtech.springlessons.buisnesobjects.*;

import java.util.Date;
import java.util.stream.Collectors;

@Service
public class Converter implements IConverter {
    @Override
    public TagObject convert(Tag tag){
        TagObject tagObject = new TagObject();
        tagObject.setK(tag.getK());
        tagObject.setV(tag.getV());
        return tagObject;
    }

    @Override
    public MemberObject convert(Member member) {
        MemberObject memberObject = new MemberObject();
        memberObject.setRef(member.getRef().longValue());
        memberObject.setRole(member.getRole());
        memberObject.setType(member.getType());
        return memberObject;
    }

    @Override
    public RelationObject convert(Relation relation) {
        RelationObject relationObject = new RelationObject();
        relationObject.setNativeId(relation.getId().longValue());
        relationObject.setVersion(relation.getVersion());
        relationObject.setTimestamp(new Date(relation.getTimestamp().toGregorianCalendar().getTimeInMillis()));
        relationObject.setTags(relation.getTagOrMember().stream()
                .filter(o -> o instanceof Tag)
                .map(tag -> convert((Tag)tag))
                .collect(Collectors.toList()));
        relationObject.setMembers(relation.getTagOrMember().stream()
                .filter(o -> o instanceof Member)
                .map(member -> convert((Member) member))
                .collect(Collectors.toList()));
        return relationObject;
    }

    @Override
    public WayObject convert(Way way) {
        WayObject wayObject = new WayObject();
        wayObject.setNativeId(way.getId().longValue());
        wayObject.setVersion(way.getVersion());
        wayObject.setTimestamp(new Date(way.getTimestamp().toGregorianCalendar().getTimeInMillis()));
        wayObject.setTags(way.getRest().stream()
                .filter(o -> o instanceof Tag)
                .map(tag -> convert((Tag) tag))
                .collect(Collectors.toList()));
        wayObject.setNdRef(way.getRest().stream()
                .filter(o -> o instanceof Nd)
                .map(nd -> ((Nd) nd).getRef().longValue())
                .collect(Collectors.toList()));
        return wayObject;
    }

    @Override
    public NodeObject convert(Node node){
        NodeObject nodeObject = new NodeObject();
        nodeObject.setNativeId(node.getId().longValue());
        nodeObject.setVersion(node.getVersion());
        nodeObject.setLat(node.getLat());
        nodeObject.setLon(node.getLon());
        nodeObject.setTimestamp(new Date(node.getTimestamp().toGregorianCalendar().getTimeInMillis()));
        nodeObject.setTags(node.getTag().stream().filter(tag -> !"amenity".equals(tag.getK())).map(this::convert).collect(Collectors.toList()));
        node.getTag().stream()
                .filter(tag -> "amenity".equals(tag.getK()))
                .findFirst()
                .ifPresent(tag -> nodeObject.setAmenity(new NodeAmenity(tag.getV())));
        return  nodeObject;
    }

    @Override
    public Object convert(Object object){
        if (object instanceof Node){
            return convert((Node) object);
        }
        if (object instanceof Way){
            return convert((Way) object);
        }
        if (object instanceof Relation){
            return convert((Relation) object);
        }
        return null;
    }
}

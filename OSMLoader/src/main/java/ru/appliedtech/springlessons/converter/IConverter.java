package ru.appliedtech.springlessons.converter;

import ru.appliedtech.osm.objects.*;
import ru.appliedtech.springlessons.buisnesobjects.*;

public interface IConverter {
    public NodeObject convert(Node node);
    public TagObject convert(Tag tag);
    public MemberObject convert(Member member);
    public RelationObject convert(Relation relation);
    public WayObject convert(Way way);
    public Object convert(Object object);
}

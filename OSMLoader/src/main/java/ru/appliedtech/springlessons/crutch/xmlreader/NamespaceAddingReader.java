package ru.appliedtech.springlessons.crutch.xmlreader;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.util.StreamReaderDelegate;

public class NamespaceAddingReader extends StreamReaderDelegate {
    private static final String NAMESPACE_URI = "http://openstreetmap.org/osm/0.6";

    public NamespaceAddingReader(XMLStreamReader reader) {
        super(reader);
    }

    @Override
    public String getNamespaceURI() {
        return NAMESPACE_URI;
    }
}

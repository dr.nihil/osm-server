package ru.appliedtech.springlessons.decompressor.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


@Slf4j
@Component
public class BZIP2Decompressor implements IDecompressor{

    @Override
    public InputStream decompressFile(String filePath) {
        try (InputStream inputStream = new FileInputStream(filePath)){
            return new BZip2CompressorInputStream(inputStream);
        } catch (FileNotFoundException e) {
            log.error("Can't find file " + filePath, e);
        } catch (IOException e){
            log.error("Can't read file " + filePath, e);
        }
        return null;
    }
}

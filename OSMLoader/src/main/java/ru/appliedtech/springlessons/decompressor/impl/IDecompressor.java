package ru.appliedtech.springlessons.decompressor.impl;

import java.io.InputStream;

public interface IDecompressor {
    public InputStream decompressFile(String filePath);
}

package ru.appliedtech.springlessons.monitoring.impl;

import ru.appliedtech.springlessons.monitoring.Monitor;
import ru.appliedtech.springlessons.monitoring.statuses.LoadStatus;
import ru.appliedtech.springlessons.monitoring.statuses.ServerStatus;
import org.springframework.stereotype.Service;

@Service
public class MonitorImpl implements Monitor {

    private final ServerStatus serverStatus = new ServerStatus();
    private final LoadStatus loadStatus = new LoadStatus();

    private MonitorImpl(){
        serverStatus.setStatus("OK");
        loadStatus.setIsRunning(false);
    }

    @Override
    public ServerStatus getServerStatus() {
        return serverStatus;
    }

    @Override
    public LoadStatus getLoadStatus() {
        return loadStatus;
    }
}

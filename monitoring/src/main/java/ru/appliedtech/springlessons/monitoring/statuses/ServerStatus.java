package ru.appliedtech.springlessons.monitoring.statuses;

import lombok.Data;

@Data
public class ServerStatus {
    private String status;
}

package ru.appliedtech.springlessons.monitoring.statuses;

public interface ILoadStatus {
    public Boolean isRunning();
    public void start();
    public void stop();
    public void increaseCounter(String element);
}

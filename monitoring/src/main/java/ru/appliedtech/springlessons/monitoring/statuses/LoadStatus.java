package ru.appliedtech.springlessons.monitoring.statuses;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Data
public class LoadStatus implements ILoadStatus {
    private Boolean isRunning = false;
    private Map<String, Long> elementCount = new HashMap<>();

    @Override
    public Boolean isRunning() {
        return isRunning;
    }

    @Override
    public void start() {
        isRunning = true;
        elementCount = new HashMap<>();
    }

    @Override
    public void stop() {
        isRunning = false;
    }

    @Override
    public void increaseCounter(String element) {
        elementCount.put(element, Optional.ofNullable(elementCount.get(element)).orElse(0L) + 1);
    }
}

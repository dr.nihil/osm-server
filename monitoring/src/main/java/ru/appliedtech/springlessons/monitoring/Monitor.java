package ru.appliedtech.springlessons.monitoring;

import ru.appliedtech.springlessons.monitoring.statuses.ILoadStatus;
import ru.appliedtech.springlessons.monitoring.statuses.ServerStatus;

public interface Monitor {
    public ServerStatus getServerStatus();
    public ILoadStatus getLoadStatus();
}
